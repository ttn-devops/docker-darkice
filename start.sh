#!/bin/bash

if [ ${REALTIME:-false} == true ]; then
  REALTIME_OPT=-R
else
  REALTIME_OPT=-r
fi

if [ ${OVERRIDE_CONF:-false} == true ]; then
    echo "Overriding conf!"
    cp /darkice/darkice.cfg /etc/darkice/darkice.cfg
    if [ -z ${SERVER+x} ]; then echo "SERVER is unset"; exit; fi
    if [ -z ${MOUNTPOINT+x} ]; then echo "MOUNTPOINT is unset"; exit; fi
fi

if [ ! -f /etc/darkice/darkice.cfg ]; then
    cp /darkice/darkice.cfg /etc/darkice/darkice.cfg
    if [ -z ${SERVER+x} ]; then echo "SERVER is unset"; exit; fi
    if [ -z ${MOUNTPOINT+x} ]; then echo "MOUNTPOINT is unset"; exit; fi
fi

sed -i 's/DURATION/'"${DURATION:-0}"'/' /etc/darkice/darkice.cfg
sed -i 's/BUFFER_SECS/'"${BUFFER_SECS:-10}"'/' /etc/darkice/darkice.cfg
sed -i 's/RECONNECT/'"${RECONNECT:-yes}"'/' /etc/darkice/darkice.cfg
sed -i 's/DEVICE/'"${DEVICE:-jack_auto}"'/' /etc/darkice/darkice.cfg
sed -i 's/SAMPLE_RATE/'"${SAMPLE_RATE:-44100}"'/' /etc/darkice/darkice.cfg
sed -i 's/BITS_PER_SAMPLE/'"${BITS_PER_SAMPLE:-16}"'/' /etc/darkice/darkice.cfg
sed -i 's/CHANNEL/'"${CHANNEL:-2}"'/' /etc/darkice/darkice.cfg
sed -i 's/BITRATE_MODE/'"${BITRATE_MODE:-cbr}"'/' /etc/darkice/darkice.cfg
sed -i 's/FORMAT/'"${FORMAT:-vorbis}"'/' /etc/darkice/darkice.cfg
sed -i 's/BITRATE/'"${BITRATE:-128}"'/' /etc/darkice/darkice.cfg
sed -i 's/QUALITY/'"${QUALITY:-0.8}"'/' /etc/darkice/darkice.cfg
sed -i 's/SERVER/'"$SERVER"'/' /etc/darkice/darkice.cfg
sed -i 's/PORT/'"${PORT:-8000}"'/' /etc/darkice/darkice.cfg
sed -i 's/PASSWORD/'"$PASSWORD"'/' /etc/darkice/darkice.cfg
sed -i 's/MOUNTPOINT/'"$MOUNTPOINT"'/' /etc/darkice/darkice.cfg
sed -i 's/NAME/'"$NAME"'/' /etc/darkice/darkice.cfg
sed -i 's/DESCRIPTION/'"$DESCRIPTION"'/' /etc/darkice/darkice.cfg
sed -i 's/WEB/'"$WEB"'/' /etc/darkice/darkice.cfg
sed -i 's/GENRE/'"$GENRE"'/' /etc/darkice/darkice.cfg
sed -i 's/PUBLIC/'"${PUBLIC:-yes}"'/' /etc/darkice/darkice.cfg

jackd $REALTIME_OPT -d net -a ${JACK_SERVER:-'127.0.0.1'} -n ${CLIENT_NAME:-'darkice'} 1> /dev/null 2> /dev/null &
sleep ${JACK_TIMEOUT:-5}
exec darkice -c /etc/darkice/darkice.cfg

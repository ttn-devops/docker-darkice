FROM registry.gitlab.com/ttn-devops/docker-jackd

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y darkice && \
    rm -rf /etc/darkice && mkdir /etc/darkice

COPY darkice.cfg /darkice/darkice.cfg
COPY start.sh /usr/local/bin/start.sh
VOLUME /etc/darkice

CMD ["start.sh"]
